# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/topics/item-pipeline.html
# REMEMBER: the actual data is in data.db
import sqlite3
from scrapy import log
import sqlite3

class CraigslistPipeline(object):
    def process_item(self, item, spider):
        return item

class DbPipeline:
    def __init__(self):
        self.conn = sqlite3.connect('/Users/willskinner/current/carstats/craigslist/data.db')
        self.cur = self.conn.cursor()

    def process_item(self, item, spider):
        query = """INSERT INTO cardata (year, model , make , price , title , body , miles , posted, updated, url, region) VALUES
                                        (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"""
        values = ( item['year'], item['model'], item['make'], item['price'], item['title'], item['body'], item['miles'], item['posted'], item['updated'], item['url'], item['region'])
        log.msg('Attempting to store item: %s' % str(item), level=log.DEBUG)
        self.cur.execute(query, values)
        self.conn.commit()
        log.msg('Item stored: %s' % str(item), level=log.DEBUG)

    def handle_error(self, e):
        log.err(e)


