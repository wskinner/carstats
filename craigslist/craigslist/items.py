# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field
import lxml.html as html
import re, json
from datetime import date
from scrapy.selector import HtmlXPathSelector

class Listing(Item):

    model = Field()
    make = Field()
    year = Field()
    price = Field()
    miles = Field()
    body = Field()
    title = Field()
    posted = Field()
    updated = Field()
    url = Field()
    region = Field()
       
    def __str__(self):
        return self['url']
