def getAllMakes(cur):
    query = """SELECT alternate FROM equivalent_makes;"""
    cur.execute(query)
    return [m[0] for m in cur.fetchall()]

