from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector
from craigslist.items import Listing
from util import *

import re
import sqlite3
from datetime import date

# cardata(year integer(4), model text, make text, price integer, title text, body text, date datetime default current_timestamp, miles integer, id integer primary key)
class MySpider(CrawlSpider):
    listing_patt = re.compile(r'craigslist.org\/[a-z]+\/cto\/[0-9]+\.html')
    name = 'craig'
    allowed_domains = ['craigslist.org']
    start_urls = []#http://sfbay.craigslist.org/cto', 
    with open('cl_root_pages.txt', 'r') as f:
        start_urls = f.read().split('\n')
    
    print 'The start URLs are:', start_urls
    rules = (Rule(SgmlLinkExtractor(allow=('(index[0-9]{1,4}\.html|/[a-z]+/cto/[0-9]+\.html)',), restrict_xpaths=(['//p[@class="nextpage"]', '//span[@class="pl"]'])), callback='parse_items', follow=True),)

    conn = sqlite3.connect('/Users/willskinner/current/carstats/craigslist/cars.db')
    conn.create_function('FIXENCODING', 1, lambda s: str(s).decode('latin-1'))
    conn.execute("UPDATE cars SET make=FIXENCODING(CAST(make AS BLOB))")
    conn.execute("UPDATE cars SET model=FIXENCODING(CAST(model AS BLOB))")
    conn.execute("UPDATE equivalent_makes SET alternate=FIXENCODING(CAST(alternate AS BLOB))")
    conn.execute("UPDATE equivalent_makes SET canonical=FIXENCODING(CAST(canonical AS BLOB))")
    conn.execute("UPDATE equivalent_models SET alternate=FIXENCODING(CAST(alternate AS BLOB))")
    conn.execute("UPDATE equivalent_models SET make=FIXENCODING(CAST(make AS BLOB))")
    conn.execute("UPDATE equivalent_models SET canonical_model=FIXENCODING(CAST(canonical_model AS BLOB))")
    cur = conn.cursor()


    title_xpath = '//h2/text()[2]' 
    body_xpath = '//section[@id="postingbody"]/text()'
    posted_xpath = '//p[@class="postinginfo"]/date/text()'

    price_patt = re.compile(r'\$([0-9]+)')
    full_year_patt = re.compile(r'(^|\s|[^\$0-9])([0-9]{4})')
    half_year_patt = re.compile(r'(^|\s|[^\$0-9])([0-9]{2})(^|\s|[^\$0-9])')
    miles_patt = re.compile(r'\s([0-9]+(,[0-9]+)?)\s*k')

    makes = getAllMakes(cur)
    make_patt = re.compile('(' + '|'.join(makes) + ')')
    
    model_patts = {}

    current_year = date.today().year % 2000

    def parse_items(self, response):
        if not self.isListing(response.url):
            print 'url', '"' + response.url + '"', 'is not listing'
            return None

        item = Listing()
        hxs = HtmlXPathSelector(response)
        title = item['title'] = hxs.select(self.title_xpath).extract()[0].lower()
        body = item['body'] = hxs.select(self.body_xpath).extract()[0].lower()
        posted = hxs.select(self.posted_xpath).extract()

        item['posted'], item['updated'] = self.getTimestamps(posted)
        item['price'] = self.makeInt(self.getPrice(title, body))
        item['year'] = self.makeInt(self.getYear(title, body))
        item['make'] = self.getMake(title, body)
        item['model'] = self.getModel(title, body, item['make'])
        item['miles'] = self.makeInt(self.getMiles(title, body))
        item['url'] = response.url
        item['region'] = self.getRegion(response.url)

        return [self.canonicalize(item)]

    def isListing(self, url):
        if re.search(self.listing_patt, url.strip()):
            return True
        return False

    def search(self, patt, title, body):
        search_results = re.search(patt, title)
        if not search_results:
            search_results = re.search(patt, body)
            if not search_results:
                return None
        return search_results

    def getPrice(self, title, body):
        search_results = self.search(self.price_patt, title, body)
        if not search_results:
            return None
        return search_results.group(1)

    def getYear(self, title, body):
        search_results = self.search(self.full_year_patt, title, body)
        if not search_results:
            search_results = self.search(self.half_year_patt, title, body)
            if not search_results:
                return None
        year = search_results.group(2)
        if len(year) == 2:
            if self.current_year < int(year):
               year = '19' + year
            else:
                year = '20' + year
        return year

    def getMake(self, title, body):
        search_results = self.search(self.make_patt, title, body)
        if not search_results:
            return None
        return search_results.group(1).strip()

    def getModel(self, title, body, make):
        if not make:
            return None

        query = """SELECT canonical_model FROM equivalent_models WHERE make=?"""
        self.cur.execute(query, [make])
        models = (m[0] for m in self.cur.fetchall())
        model_patt = self.model_patts.get(make)
        if not model_patt:
            model_patt = self.model_patts[make] = '(' + '|'.join(models) + ')'

        search_results = self.search(model_patt, title, body)
        if not search_results:
            return None
        return search_results.group(1).strip()

    def getMiles(self, title, body):
        search_results = self.search(self.miles_patt, title, body)
        if not search_results:
            return None
        return search_results.group(1).strip() + '000'

    def getTimestamps(self, timestamp_list):
        posted = timestamp_list[0]
        updated = None
        if len(timestamp_list) > 2:
            updated =  timestamp_list[len(timestamp_list) - 1]
        return (posted, updated)
 
    def getRegion(self, url):
        return url.split('/')[1]

    def makeInt(self, el):
        if el is not None:
            return int(el)
        return el

    def canonicalize(self, item):
        if item.get('make'):
            query = """SELECT canonical FROM equivalent_makes WHERE alternate=?;""" 
            self.cur.execute(query, [item['make']])
            results = self.cur.fetchall()
            if len(results) > 0:
                item['make'] = results[0][0]
            else:
                item['make'] = 'NONE'
        else:
            item['make'] = 'NONE'

        if item['make'] != 'NONE' and item.get('model'):
            query = """SELECT canonical_model FROM equivalent_models WHERE alternate=? AND make=?;""" 
            self.cur.execute(query, (item['model'], item['make']))
            results = self.cur.fetchall()
            if len(results) > 0:
                item['model'] = results[0][0]
            else:
                item['model'] = 'NONE'
        else:
            item['model'] = 'NONE'

        if item['make'] != 'NONE' and item.get('model'):
            query = """SELECT canonical_model FROM equivalent_models WHERE alternate=? AND make=?;""" 
            self.cur.execute(query, (item['model'], item['make']))
            results = self.cur.fetchall()
            if len(results) > 0:
                item['model'] = results[0][0]
            else:
                item['model'] = 'NONE'
        else:
            item['model'] = 'NONE'
        return item


