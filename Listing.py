import lxml.html as html
import re
from datetime import date
from scrapy.selector import HtmlXPathSelector

class Listing:
    title_xpath = '//h2/text()[2]' 
    body_xpath = '//section[@id="postingbody"]/text()'

    price_patt = r'\$([0-9]+)'
    full_year_patt = r'(^|\s|[^\$0-9])([0-9]{4})'
    half_year_patt = r'(^|\s|[^\$0-9])([0-9]{2})(^|\s|[^\$0-9])'
    miles_patt = r'([0-9]+(,[0-9]+)?)\s*k'

    makes = ['ford', 'subaru', 'mercedes-benz', 'mercedes', 'honda', 'toyota', 'chevrolet', 'chevy']
    make_patt = '(' + '|'.join(makes) + ')'

    current_year = date.today().year % 2000

    synonymous_makes = {'chevy': 'chevrolet',
                        'mercedes': 'mercedes-benz'}
    
    models = {'subaru': ['impreza', 'impreza wrx', 'impreza wrx sti', 'wrx sti', 'outback', 'forester', 'wrx', 'sti']}
    for make in makes:
        if make not in models:
            models[make] = []
    synonymous_models = {'subaru': {'wrx': 'impreza wrx', 'sti': 'impreza wrx sti', 'wrx sti': 'impreza wrx sti'}}
     
    def __init__(self, response):
        self.body = response.body
        hxs = HtmlXPathSelector(response)
        self.title = hxs.select(title_xpath).extract()
        self.body = hxs.select(body_xpath).extract()
        self.build()

    def getTitle(self):
        self.title = self.element.xpath(self.title_xpath)[0].strip().lower()

    def getBody(self):
        self.body = ''.join(self.element.xpath(self.body_xpath)).strip().lower()

    def getPrice(self):
        search_results = re.search(self.price_patt, self.title)
        if search_results:
            self.price = search_results.group(1)

    def getYear(self):
        search_results = re.search(self.full_year_patt, self.title)
        if not search_results:
            search_results = re.search(self.half_patt, self.title)
            if not search_results:
                print 'no year found'
                self.year = None
                return
        year = search_results.group(2)
        if len(year) == 2:
            if self.current_year < int(year):
                self.year = '19' + year
            else:
                self.year = '20' + year
        else:
            self.year = year

    def getMake(self):
        search_results = re.search(self.make_patt, self.title)
        # try body
        if not search_results:
            search_results = re.search(self.make_patt, self.body)
            if not search_results:
                self.make = None
                return
        self.make = search_results.group(1).strip()

    def getModel(self):
        if not self.make:
            self.model = None
            return
        model_patt = '(' + '|'.join(self.models[self.make]) + ')'
        search_results = re.search(model_patt, self.title)
        if not search_results:
            search_results = re.search(model_patt, self.body)
            if not search_results:
                self.model = None
                return
        self.model = search_results.group(1).strip()

    def getMiles(self):
        search_results = re.search(self.miles_patt, self.title)
        if not search_results:
            search_results = re.search(self.miles_patt, self.body)
            if not search_results:
                self.miles = None
                return
        self.miles = search_results.group(1).strip() + '000'
        
    def build(self, url):
        self.getElement(url)
        self.getTitle()
        self.getBody()
        self.getYear()
        self.getPrice()
        self.getMake()
        self.getModel()
        self.getMiles()

